"use strict";
application
	.config(function(
		$httpProvider, 
		$locationProvider, 
		$mdThemingProvider, 
		$mdGestureProvider, 
		$mdDateLocaleProvider
	) {
		$locationProvider.html5Mode(true);
		$httpProvider.interceptors.push('httpInterceptor');
		$mdGestureProvider.skipClickHijack();

		$mdDateLocaleProvider.formatDate = function(date) {
	    	moment.locale('pt-BR');
	    	return date ? moment(date).format('DD/MM/YYYY') : '';
	    };
	    $mdDateLocaleProvider.parseDate = function(dateString) {
	    	moment.locale('pt-BR');
			var m = moment(dateString, "DD/MM/YYYY H:m:s");
			return m.isValid() ? m.toDate() : new Date(NaN);
		};
		
		var themingProvider = $mdThemingProvider.theme('default')
    		.primaryPalette('indigo')
    		.accentPalette('orange')
    		.warnPalette('red');
    	
    	themingProvider.foregroundPalette[3] = "rgba(0,0,0,.4)";
    	themingProvider.foregroundPalette[4] = "rgba(0,0,0,.8)";
	})
	.value('config', {
	    service_url : "",
	});