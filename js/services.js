"use strict";
application
	.service('httpInterceptor', ['$rootScope', function($rootScope) {
		return {
			request: function(config) {
				$rootScope.$broadcast('request');
				return config;
			},
			requestError: function(rejection) {
				$rootScope.$broadcast('error', rejection.data);
				return rejection;
			},
			responseError: function(rejection) {
				if(rejection.status == 401 || (rejection.data && rejection.data.message && rejection.data.message.indexOf('Unauthenticated') >= 0 )) {
					delete rejection.data.message;
					$rootScope.$broadcast('unauthorized', rejection.data, rejection.status);
				}
				else {
					$rootScope.$broadcast('error', rejection.data, rejection.status);
				}
				return rejection;
			},
			response: function(response) {
				$rootScope.$broadcast('response');
				return response;
			},
		};
	}]);