"use strict";
application
	.directive('dateFormat', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				var elementoMask;
				if(angular.element(element).find('input').length)
		        	elementoMask = angular.element(element).find('input');
		        else 
		        	elementoMask = angular.element(element);
				elementoMask.mask('00/00/0000');
			}
		}
	})
	.directive('dateTimeFormat', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				var elementoMask;
				if(angular.element(element).find('input').length)
		        	elementoMask = angular.element(element).find('input');
		        else 
		        	elementoMask = angular.element(element);
				elementoMask.mask('00/00/0000 00:00');
			}
		}
	})
	.directive('scaleImage', function($window, config) {
		return {
			restrict : 'A',
			scope: {
	            src : "@src"
	        },
			link: function(scope, element, attrs) {
				var w = angular.element($window);
		        w.bind('resize', function () {
					element.height((attrs.scaleImage / 100) * element.width());
					if(attrs.src) {
						element.css('background-image', 'url(' + attrs.src + ')');
					} else {
						element.css('background-image', '');
					}
		        });
				element.ready(function(){
			        w.triggerHandler('resize');
				});
				attrs.$observe('src', function () {
					w.triggerHandler('resize');
				});
			}
		}
	})
	.directive('intFormat', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				// Convert model to view
				ngModelController.$formatters.push(function(data) {
					if(data)
						return parseInt(data.toString().replace(/\D/g, ''));
					return data;
				});
				// Mask
				var maskValue = '000000';
				if(attrs.numberFormat) {
					maskValue = '';
					for (var i = 0; i < attrs.numberFormat; i++) {
						maskValue += '0';
					}
				}
				angular.element(element).mask(maskValue);
				// Convert view to model
				ngModelController.$parsers.push(function(data) {
	    			if(data)
	      				data = data.toString().replace(/\D/g, '');
	    			return parseInt(data);
	  			});
			}
		}
	})
	.directive('numberFormat', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				// Convert model to view
				ngModelController.$formatters.push(function(data) {
					if(data)
						return data.toString().replace(/\D/g, '');
					return data;
				});
				// Mask
				var maskValue = '000000';
				if(attrs.numberFormat) {
					maskValue = '';
					for (var i = 0; i < attrs.numberFormat; i++) {
						maskValue += '0';
					}
				}
				
				angular.element(element).mask(maskValue);

				// Convert view to model
				ngModelController.$parsers.push(function(data) {
	    			if(data)
	      				data = data.toString().replace(/\D/g, '');
	    			return data;
	  			});
			}
		}
	})
	.directive('fill', function($window) {
		return {
			restrict: 'A',
			link: function(scope, element) {
				function getComputedStyle (element, styleProp) {
					var r = '';
					if (element.currentStyle) {
						r = element.currentStyle[styleProp];
					} else if ($window.getComputedStyle) {
						r = $window.getComputedStyle(element,null).getPropertyValue(styleProp);
					}        
					return r;
				}
				function getComputedSize(element, styleProp) {
					var r =  getComputedStyle (element, styleProp);
					if (r.indexOf('px')>0) r = r.substring(0,r.length-2);
					return r|0;
				}
				function getRestHeights (element) {
					var before = true;
					var total = 0;
					var pChildren = element.parentNode.childNodes;
					for (var i = 0;i<=pChildren.length;i++) {
						if (before) before = pChildren[i]!=element;
						else {              
							if (pChildren[i]) {
								if (pChildren[i].nodeType==1) 
									getComputedStyle(pChildren[i],'display');
								if (
									pChildren[i].nodeType==1 
									&& getComputedStyle(pChildren[i],'display')!='none'
									&& getComputedStyle(pChildren[i],'position')!='absolute' 
									)
									total += pChildren[i].scrollHeight|0;
							}
						}
					}
					return total;
				}
				function calcBottomPaddings(element) {
					var total = 0;
					total += getComputedSize(element,'padding-bottom');
					total += getComputedSize(element,'margin-bottom');        
					if (element.parentNode && element.parentNode.nodeType == 1) {
						total += calcBottomPaddings(element.parentNode);
						total += getRestHeights(element);
					}
					return total;        
				}
				function applySize() {
					var height = 0;
					var body = window.document.body;
					if (window.innerHeight) {
						height = window.innerHeight;
					} else if (body.parentElement.clientHeight) {
						height = body.parentElement.clientHeight;
					} else if (body && body.clientHeight) {
						height = body.clientHeight;
					}
					var paddings = calcBottomPaddings(element[0]);
					var resultHeight =  (height - element[0].offsetTop - paddings);
					element[0].style.height = resultHeight + "px";
				}
				angular.element($window).bind('resize', applySize);
				var intervalCount = 1;
				var interval = setInterval(function(){
					applySize();
					if(intervalCount == 3)
						clearInterval(interval);
					intervalCount++;
				}, 300);
				applySize();
				scope.$on('destroy', function() {
					angular.element($window).unbind('resize', applySize);
				})
			}
		}
	})
	.directive('whenScrolled', function() {
	    return function(scope, elm, attr) {
	        var raw = elm[0];
	        var oldScrollHeight = 0
	        elm.bind('scroll', function() {
	            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight - raw.offsetHeight && oldScrollHeight != raw.scrollHeight) {
	            	oldScrollHeight = raw.scrollHeight;
	            	scope.$apply(attr.whenScrolled);
	            }
	        });
	    };
	})
	.directive('subHeaderBlur', function() {
	    return function(scope, elm, attr) {
	        var raw = elm[0];
	    	angular.element('input, textarea').focusin(function(){
	    		var inputFocus = angular.element(this);
		        function scroll () {
		        	if(inputFocus.offset().top < angular.element('.top-toolbar').height() + inputFocus.height())
		        		angular.element('input, textarea').blur();
		        }
		    	inputFocus.focusout(function(){
		    		elm.unbind('scroll', scroll);
		    	});
		        elm.bind('scroll', scroll);
	    	});
	    };
	})
	.directive('displayAfterFocus', function($timeout) {
	    return function(scope, elm, attr) {
	    	var input;
			var startHeight;
			var resizePromise;
			var container;
			var startScrollTop;
	    	function resizing () {
	    		container = angular.element('body');
				resizePromise = $timeout(function() {
					input.off('change keyup');
					if(input.offset().top > angular.element('body').height()) {
						angular.element('md-content[md-scroll-y]').animate({
							scrollTop : startScrollTop + (startHeight - container.height()) + 20
						});
					}
				}, 1500);
	    	}
	        angular.element('input, textarea').focus(function(event) {
				input = angular.element(this);
				startHeight = angular.element('body').height();
				startScrollTop = angular.element('md-content[md-scroll-y]')[0].scrollTop - angular.element('md-content[md-scroll-y]').offset().top;
				resizing();
				input.on('change keyup', function() {
					$timeout.cancel(resizePromise);
				});
			});
	    };
	})
	.directive('onLongPress', function($timeout) {
		return {
			restrict: 'A',
			link: function($scope, $elm, $attrs) {
				$elm.bind('touchstart', function(evt) {
					// Locally scoped variable that will keep track of the long press
					$scope.longPress = true;

					// We'll set a timeout for 600 ms for a long press
					$timeout(function() {
						if ($scope.longPress) {
							// If the touchend event hasn't fired,
							// apply the function given in on the element's on-long-press attribute
							$scope.$apply(function() {
								$scope.$eval($attrs.onLongPress)
							});
						}
					}, 600);
				});

				$elm.bind('touchend', function(evt) {
					// Prevent the onLongPress event from firing
					$scope.longPress = false;
					// If there is an on-touch-end function attached to this element, apply it
					if ($attrs.onTouchEnd) {
						$scope.$apply(function() {
							$scope.$eval($attrs.onTouchEnd)
						});
					}
				});
			}
		};
	})
	.directive('isLanguage', function() {
		return {
			restrict: 'A',
			link: function(rootScope, scope, element, attrs) {
				if(rootScope.language != element.isLanguage) {
					angular.element(element.$$element[0]).hide();
				}
			}
		}
	})
	.directive("fileInputChange",function(){
    	return {
        	scope:{
            	fileInputChange:"&"
        	},
        	link:function($scope, $element, $attrs){
	            $element.on("change",function(event){
	                $scope.$apply(function(){
	                    $scope.fileInputChange({$event: event})
	                })
	            })
	            $scope.$on("$destroy",function(){
	                $element.off();
	            });
        	}
    	}
    })
	.directive('uploadImage', function($http, $rootScope, config) {
		return {
			restrict: 'E',
			scope: {
	            startUpload : '&startUpload',
	            error 	: '&error',
	            success : '&success',
	        },
	        replace 	: true,
	        
	        template 	: 	'<div layout="row" layout-align="start center" ng-disabled="isDisabled">' +
		        				'<input class="ng-hide" id="fileInput" type="file" />' +
								'<label id="uploadButton" for="fileInput" class="md-button">' +
									'<md-icon class="mdi mdi-camera"></md-icon>' +
				                    ' {{ buttonLabel }} ' +
				                '</label>' +
				            '</div>',
						
			link: function($scope, $element, $attrs) {
				var fileInput 	= $element.find('#fileInput');
				var button 		= $element.find('#uploadButton');
				$scope.buttonLabel = '';

		  		fileInput.on('change', function(event) {
		    		if (event.target.files && event.target.files.length) {
		    			var formData = new FormData();
					    formData.append("photo", event.target.files[0]);
					    $scope.isDisabled = true;
					    $rootScope.blockErrorDialog = true;
					    button.removeClass('md-accent');
					    button.removeClass('md-warn');
						if($scope.startUpload()) {
							$scope.startUpload()();
						}
						
						$scope.buttonLabel = "(" + zeroPad(event.target.files.length, 2) + ")";
					    $http({
							headers : {
								'Accept' 		: 'application/json',
								'Authorization' : localStorage.getItem('authorization'),
								'Content-Type'	: undefined 
							},
							method  			: 'POST',
							url 				: config.service_url + $attrs.endPoint,
							transformRequest 	: angular.identity,
							data 				: formData,
						}).then(function(response) {
							if(response.status == 200) {
								if($scope.success()) {
									$scope.success()(response.data);
								}
								button.addClass('md-accent');
							} else {
								if($scope.error()) {
									$scope.error()(crackMessages(response.data));
								}
								button.addClass('md-warn');
							}
							$rootScope.blockErrorDialog = false;
							$scope.isDisabled = false;
						});
					}
		    		$scope.$apply();
		  		});
			},
		};
	})
	.directive('httpsValidation', function (){ 
		return {
            restrict	: 'A',
			require		: 'ngModel',
				link: function(scope, elem, attr, ngModel) {
					ngModel.$parsers.unshift(function(value) {
						ngModel.$setValidity('httpsValidation', value == undefined || value.indexOf("http://") < 0);
						return value;
					});
					ngModel.$formatters.unshift(function(value) {
						var regex = /(src=|url\()(\"|\')http\:\/\//gi;
					    ngModel.$setValidity('httpsValidation', value == undefined || value.indexOf("http://") < 0);
						return value;
					});
				}
			};
	})
;
