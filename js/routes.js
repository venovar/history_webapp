"use strict";
application
    .config(function($stateProvider, $urlRouterProvider, $locationProvider) 
    {
        if(localStorage.getItem('account'))
            $urlRouterProvider.otherwise('/highlight/index');
        else
            $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('404', {
                views : {
                    'content@' : {
                        templateUrl     : 'views/404.html?v=' + Math.random(),
                    }
                },
                url     : '/404',
            })
            .state('login', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/login/index.html?v=' + Math.random(),
                        controller      : 'LoginController',
                    },
                },
                url : '/?code',
            })
            /// Destaques
            .state('highlightIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/highlight/index.html?v=' + Math.random(),
                        controller      : 'HighlightIndexController',
                    },
                },
                url : '/highlight/index',
            })
            .state('highlightStore', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/highlight/store.html?v=' + Math.random(),
                        controller      : 'HighlightStoreController',
                    },
                },
                url : '/highlight/store',
            })
            .state('highlightShow', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/highlight/show.html?v=' + Math.random(),
                        controller      : 'HighlightShowController',
                    },
                },
                url : '/highlight/show/:id',
            })
            .state('highlightUpdate', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/highlight/update.html?v=' + Math.random(),
                        controller      : 'HighlightUpdateController',
                    },
                },
                url : '/highlight/update/:id',
            })
            /// Gif
            .state('videoIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/video/index.html?v=' + Math.random(),
                        controller      : 'VideoIndexController',
                    },
                },
                url : '/video/index',
            })
            .state('videoStore', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/video/store.html?v=' + Math.random(),
                        controller      : 'VideoStoreController',
                    },
                },
                url : '/video/store',
            })
            .state('videoShow', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/video/show.html?v=' + Math.random(),
                        controller      : 'VideoShowController',
                    },
                },
                url : '/video/show/:id',
            })
            .state('videoUpdate', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/video/update.html?v=' + Math.random(),
                        controller      : 'VideoUpdateController',
                    },
                },
                url : '/video/update/:id',
            })
            /// Playlist
            .state('playlistIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/playlist/index.html?v=' + Math.random(),
                        controller      : 'PlaylistIndexController',
                    },
                },
                url : '/playlist/index',
            })
            .state('playlistStore', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/playlist/store.html?v=' + Math.random(),
                        controller      : 'PlaylistStoreController',
                    },
                },
                url : '/playlist/store',
            })
            .state('playlistShow', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/playlist/show.html?v=' + Math.random(),
                        controller      : 'PlaylistShowController',
                    },
                },
                url : '/playlist/show/:id',
            })
            .state('playlistUpdate', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/playlist/update.html?v=' + Math.random(),
                        controller      : 'PlaylistUpdateController',
                    },
                },
                url : '/playlist/update/:id',
            })
            /// Gif
            .state('gifIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/gif/index.html?v=' + Math.random(),
                        controller      : 'GifIndexController',
                    },
                },
                url : '/gif/index',
            })
            .state('gifStore', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/gif/store.html?v=' + Math.random(),
                        controller      : 'GifStoreController',
                    },
                },
                url : '/gif/store',
            })
            .state('gifShow', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/gif/show.html?v=' + Math.random(),
                        controller      : 'GifShowController',
                    },
                },
                url : '/gif/show/:id',
            })
            .state('gifUpdate', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/gif/update.html?v=' + Math.random(),
                        controller      : 'GifUpdateController',
                    },
                },
                url : '/gif/update/:id',
            })
            // Curiosity
            .state('curiosityIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/curiosity/index.html?v=' + Math.random(),
                        controller      : 'CuriosityIndexController',
                    },
                },
                url : '/curiosity/index',
            })
            .state('curiosityStore', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/curiosity/store.html?v=' + Math.random(),
                        controller      : 'CuriosityStoreController',
                    },
                },
                url : '/curiosity/store',
            })
            .state('curiosityShow', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/curiosity/show.html?v=' + Math.random(),
                        controller      : 'CuriosityShowController',
                    },
                },
                url : '/curiosity/show/:id',
            })
            .state('curiosityUpdate', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/curiosity/update.html?v=' + Math.random(),
                        controller      : 'CuriosityUpdateController',
                    },
                },
                url : '/curiosity/update/:id',
            })
            // Article
            .state('articleIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/article/index.html?v=' + Math.random(),
                        controller      : 'ArticleIndexController',
                    },
                },
                url : '/article/index',
            })
            .state('articleStore', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/article/store.html?v=' + Math.random(),
                        controller      : 'ArticleStoreController',
                    },
                },
                url : '/article/store',
            })
            .state('articleShow', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/article/show.html?v=' + Math.random(),
                        controller      : 'ArticleShowController',
                    },
                },
                url : '/article/show/:id',
            })
            .state('articleUpdate', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/article/update.html?v=' + Math.random(),
                        controller      : 'ArticleUpdateController',
                    },
                },
                url : '/article/update/:id',
            })
            // Serie
            .state('serieIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/serie/index.html?v=' + Math.random(),
                        controller      : 'SerieIndexController',
                    },
                },
                url : '/serie/index',
            })
            .state('serieStore', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/serie/store.html?v=' + Math.random(),
                        controller      : 'SerieStoreController',
                    },
                },
                url : '/serie/store',
            })
            .state('serieShow', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/serie/show.html?v=' + Math.random(),
                        controller      : 'SerieShowController',
                    },
                },
                url : '/serie/show/:id',
            })
            .state('serieUpdate', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/serie/update.html?v=' + Math.random(),
                        controller      : 'SerieUpdateController',
                    },
                },
                url : '/serie/update/:id',
            })
            // Institutional
            .state('institutionalIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/institutional/index.html?v=' + Math.random(),
                        controller      : 'InstitutionalIndexController',
                    },
                },
                url : '/institutional/index',
            })
            .state('institutionalStore', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/institutional/store.html?v=' + Math.random(),
                        controller      : 'InstitutionalStoreController',
                    },
                },
                url : '/institutional/store',
            })
            .state('institutionalShow', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/institutional/show.html?v=' + Math.random(),
                        controller      : 'InstitutionalShowController',
                    },
                },
                url : '/institutional/show/:id',
            })
            .state('institutionalUpdate', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/institutional/update.html?v=' + Math.random(),
                        controller      : 'InstitutionalUpdateController',
                    },
                },
                url : '/institutional/update/:id',
            })
            // User
            .state('userIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/user/index.html?v=' + Math.random(),
                        controller      : 'UserIndexController',
                    },
                },
                url : '/user/index',
            })
            .state('userShow', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/user/show.html?v=' + Math.random(),
                        controller      : 'UserShowController',
                    },
                },
                url : '/user/show/:id',
            })
            .state('appLogoStore', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'menuBar@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/appLogo/store.html?v=' + Math.random(),
                        controller      : 'AppLogoStoreController',
                    },
                },
                url : '/app_logo/store',
            })
        ;
    });