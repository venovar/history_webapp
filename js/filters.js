"use strict";
application
	/// Zero a esquerda.
	.filter('zeroPad', function () {
		return zeroPad;
	})
	.filter('datetime', function ($filter) {
		return function (value, params) {
			if(!value)
				return '';
			if(!params) {
				if(value.length <= 10)
					params = 'DD/MM/YYYY';
				else
					params = 'DD/MM/YYYY HH:mm:ss';
			}
			moment.locale('pt-BR');
			return moment(value, "YYYY-MM-DD H:m:s").format(params);
		};
	})
	.filter('service_url', function (config) {
		return function (value) {
			if (!value) { return ''; }
			return config.service_url + '/' + value;
		};
	})
	.filter('trust', function ($sce) {
		return function(value) {
			if(value && value.length > 0 && value.indexOf('http') == 0) {
				return $sce.trustAsResourceUrl(value);
			} else if(value && value.length > 0) {
	        	return $sce.trustAsHtml(value);
			}
			return value;
	    };
	});