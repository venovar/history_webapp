"use strict";
application
	.controller('PlaylistShowController', function (
		$sce,
		$http,
		$state,
		$scope,
		$stateParams,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.selectedMode = 'md-scale';
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/playlist/show',
				params	: {
					id : $stateParams.id
				}
			}).then(function(response) {
				if(response.status == 200) {
					var playlist = response.data.playlist;
					$scope.playlist = playlist;
				}
			});
		}
		show();
		
		$scope.delete = function () {
			
			confirm('Deseja realmente remover esta Playlist?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/playlist/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.playlist,
				}).then(function(response) {
					if(response.status == 200) {
						alert(response.data.message, function() {
							$state.go('playlistIndex');
						}, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
			
		}
		
		$scope.videos = function () {
			$mdDialog.show({
				controller 			: PlaylistVideoIndexController,
				templateUrl 		: 'views/page/playlist/dialog/video/index.html?v=' + Math.random(),
				clickOutsideToClose : true,
				locals : {
					playlist_id : $stateParams.id
				}
			});
		}
	})
;