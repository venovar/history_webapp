"use strict";
application
	.controller('PlaylistUpdateController', function (
		$http,
		$state,
		$scope,
		$stateParams,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.playlist = {};
		
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/playlist/show',
				params	: {
					id 			: $stateParams.id,
					markdown 	: 0,
				}
			}).then(function(response) {
				if(response.status == 200) {
					$scope.playlist = response.data.playlist;
				}
			});
		}
		show();
		$scope.imageCropDialog = function () {
			$mdDialog.show({
				controller 			: ImageCropController,
				templateUrl 		: 'views/dialog/imageCrop.html?v=' + Math.random(),
				clickOutsideToClose : true,
			})
			.then(function(image){
				$scope.playlist.image = image;
			})
			.then(function(){});
		}
		$scope.removeImage = function () {
			confirm('Deseja realmente remover esta imagem?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/image/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.playlist.image,
				}).then(function(response) {
					if(response.status == 200) {
						delete $scope.playlist.image;
						alert(response.data.message, null, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
		$scope.submit = function () {
			$http({
				url 				: config.service_url + '/api/v1/pt/cms/playlist/update',
				method  			: 'PUT',
				headers : {
					'Accept' 		: 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				data 				: $scope.playlist,
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						$state.go('playlistIndex');
					}, 'Sucesso');
				}
			});
		}
	})
;