"use strict";
function PlaylistVideoIndexController (
		$http,
		$state,
		$scope,
		$rootScope,
		$mdDialog,
		playlist_id,
		config) {
	
	$scope.filter = {
		author : 1
	}
    
	$scope.index = function (start) {
		if(start) {
			$scope.page = 1;
			$scope.videos = [];
		}
		$http({
			headers : {
				'Accept' : 'application/json',
				'Authorization' : localStorage.getItem('authorization')
			},
			method  : 'GET',
			url 	: config.service_url + '/api/v1/pt/cms/playlist/video/index',
			params 	: {
				page 		: $scope.page,
				playlist_id : playlist_id,
				author 		: $scope.filter.author,
			}
		}).then(function(response) {
			if(response.status == 200) {
				$scope.videos = $scope.videos.concat(response.data.videos.data);
				$scope.total = response.data.videos.total;
				if(response.data.videos.next_page_url != null) {
					$scope.page++; 
				} else {
					$scope.page = 0;
				}
			}
		});
	}
	
	$scope.cancel = function () {
    	$mdDialog.cancel();
    }
    
    $scope.highlight = function (id) {
    	
    	for(var prop in $scope.videos) {
    		var video = $scope.videos[prop];
    		if(video.id == id || (video.playlists && video.playlists.length > 0 && video.playlists[0].pivot.highlight == 1)) {
    			video.playlists = [
    				{ 
    					pivot : {
    						highlight : video.playlists && video.playlists.length && video.playlists[0].pivot.highlight == 1 ? 0:1
    					}
    				}
    			];
    		}
    	}
    }
    
    $scope.add = function (video) {
    	
		if(video.playlists && video.playlists.length > 0) {
			video.playlists = [];
		} else {
			video.playlists = [
				{ 
					pivot : {
						highlight : 0
					}
				}
			];
		}
    }
	
	$scope.hide = function () {
    	var videos = [];
    	for(var prop in $scope.videos) {
    		var video = $scope.videos[prop];
    		if(video.playlists && video.playlists.length > 0) {
	    		videos.push({
	    			id : video.id,
	    			highlight : video.playlists && video.playlists.length > 0 && video.playlists[0].pivot.highlight == 1
	    		});
    		}
    	}
    	$http({
			headers : {
				'Accept' : 'application/json',
				'Authorization' : localStorage.getItem('authorization')
			},
			method  : 'PUT',
			url 	: config.service_url + '/api/v1/pt/cms/playlist/video/update',
			data 	: {
				page 		: $scope.page,
				playlist_id : playlist_id,
				videos		: videos,
			}
		}).then(function(response) {
			if(response.status == 200) {
				$mdDialog.hide()
				alert(response.data.message, null, 'Sucesso');
			}
		});
    }
    
	$scope.index(true);
};