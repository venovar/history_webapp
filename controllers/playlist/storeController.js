"use strict";
application
	.controller('PlaylistStoreController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.playlist = {};
		
		$scope.imageCropDialog = function () {
			$mdDialog.show({
				controller 			: ImageCropController,
				templateUrl 		: 'views/dialog/imageCrop.html?v=' + Math.random(),
				clickOutsideToClose : true,
			})
			.then(function(image){
				$scope.playlist.image = image;
			})
			.then(function(){});
		}
		
		$scope.removeImage = function () {
			
			confirm('Deseja realmente remover esta imagem?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/image/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.playlist.image,
				}).then(function(response) {
					if(response.status == 200) {
						delete $scope.playlist.image;
						alert(response.data.message, null, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
			
		}
		
		function goPlaylistShow(id) {
			$state.go('playlistShow', { id : id });
		}
		
		$scope.submit = function () {
			$http({
				url 				: config.service_url + '/api/v1/pt/cms/playlist/store',
				method  			: 'POST',
				headers : {
					'Accept' 		: 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				data 				: $scope.playlist,
			}).then(function(response) {
				if(response.status == 200) {
					confirm(response.data.message + ' Deseja adicionar vídeos as esta Playlist?', 
						function() {
							$mdDialog.show({
								controller 			: PlaylistVideoIndexController,
								templateUrl 		: 'views/page/playlist/dialog/video/index.html?v=' + Math.random(),
								clickOutsideToClose : true,
								locals : {
									playlist_id : response.data.playlist.id
								}
							}).then(
							function() {
								goPlaylistShow(response.data.playlist.id);
							},
							function() {
								goPlaylistShow(response.data.playlist.id);
							});
						}, 
						function() {
							goPlaylistShow(response.data.playlist.id);
						}, 
						'Sucesso', 
						'Sim', 'Não');
				}
			});
		}
	})
;