"use strict";
application
	.controller('PlaylistIndexController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		config) 
	{
		$scope.filter = {
			author : 0
		}
		var filterSubmited;
		$scope.index = function (start, force) {
			if(start) {
				$scope.page = 1;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited))
					$scope.playlists = [];
				if(force) {
					$scope.playlists = [];
					filterSubmited = null;
				}
			}
			if($scope.page > 0) {
				$rootScope.inBackground = true;
				$scope.filter.page = $scope.page;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited)) {
					filterSubmited = angular.copy($scope.filter);
					$http({
						headers : {
							'Accept' : 'application/json',
							'Authorization' : localStorage.getItem('authorization')
						},
						method  : 'GET',
						url 	: config.service_url + '/api/v1/pt/cms/playlist/index',
						params 	: $scope.filter
					}).then(function(response) {
						$rootScope.inBackground = false;
						if(response.status == 200) {
							$scope.playlists = $scope.playlists.concat(response.data.playlists.data);
							$scope.total = response.data.playlists.total;
							if(response.data.playlists.next_page_url != null) {
								$scope.page++; 
							} else {
								$scope.page = 0;
							}
						}
					});
				}
			}
		}
		$scope.index(true);
		
		$scope.delete = function (playlist) {
			
			confirm('Deseja realmente remover esta Playlist?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/playlist/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: playlist,
				}).then(function(response) {
					if(response.status == 200) {
						$scope.index(true, true);
						alert(response.data.message, null, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
			
		}
	})
;