"use strict";
application
	.controller('HighlightStoreController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$mdDialog,
		config) 
	{
		var lastAttachmentSubmited;
		$scope.highlight = {};
		function dataprovider () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/highlight/dataprovider'
			}).then(function(response) {
				if(response.status == 200) {
					$scope.attachment_types = response.data.attachment_types;
				}
			});
		}
		dataprovider();
		$scope.imageCropDialog = function () {
			$mdDialog.show({
				controller 			: ImageCropController,
				templateUrl 		: 'views/dialog/imageCrop.html?v=' + Math.random(),
				clickOutsideToClose : true,
			})
			.then(function(image){
				$scope.highlight.image = image;
			})
			.then(function(){});
		}
		$scope.removeImage = function () {
			confirm('Deseja realmente remover esta imagem?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/image/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.highlight.image,
				}).then(function(response) {
					if(response.status == 200) {
						delete $scope.highlight.image;
						alert(response.data.message, null, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
		$scope.checkAttachment = function () {
			delete $scope.check_attachment;
			if($scope.highlight.attachment_type_id && 
				$scope.highlight.attachment_id &&
				lastAttachmentSubmited != $scope.highlight.attachment_id ) {
				$rootScope.inBackground = true;
				lastAttachmentSubmited = $scope.highlight.attachment_id;
				$http({
					headers : {
						'Accept' : 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					method  : 'GET',
					url 	: config.service_url + '/api/v1/pt/cms/highlight/attachment/check',
					params 	: $scope.highlight,
				}).then(function(response) {
					$rootScope.inBackground = false;
					if(response.status == 200 && response.data) {
						$scope.check_attachment = response.data;
					}
				});
			}
		}
		$scope.submit = function () {
			$http({
				url 				: config.service_url + '/api/v1/pt/cms/highlight/store',
				method  			: 'POST',
				headers : {
					'Accept' 		: 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				data 				: $scope.highlight,
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						$state.go('highlightIndex');
					}, 'Sucesso');
				}
			});
		}
	})
;