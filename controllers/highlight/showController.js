"use strict";
application
	.controller('HighlightShowController', function (
		$http,
		$state,
		$scope,
		$stateParams,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.selectedMode = 'md-scale';
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/highlight/show',
				params	: {
					id : $stateParams.id
				}
			}).then(function(response) {
				if(response.status == 200) {
					$scope.highlight = response.data.highlight;
				}
			});
		}
		show();
		
		$scope.delete = function () {
			
			confirm('Deseja realmente remover este Destaque?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/highlight/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.highlight,
				}).then(function(response) {
					if(response.status == 200) {
						alert(response.data.message, function() {
							$state.go('highlightIndex');
						}, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
			
		}
		
		$scope.goAttach = function(type, id) {
			if(type)
				$state.go(type.split('\\')[1].toLowerCase() + 'Show', { id : id });
		}
	})
;