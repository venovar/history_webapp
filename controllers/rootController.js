"use strict";
application
	.controller('RootController', function (
		$http,
		$state,
		$window,
		$timeout,
		$mdDialog,
		$mdSidenav,
		$rootScope,
		$location,
		config,
		mdcDefaultParams) 
	{	
		var requestResponseStack = [];
		var accountJson = localStorage.getItem('account');
    	if($location.absUrl().indexOf('history.venovar.com.br') >= 0) {
    		config.service_url = "https://local.history.venovar.com.br";
    	} else if($location.absUrl().indexOf('history.14bits.venovar.com.br') >= 0) {
    		config.service_url = "https://service.history.14bits.venovar.com.br";
    	} else if($location.absUrl().indexOf('dev-making.seuhistory.com') >= 0) {
    		config.service_url = "https://dev-api-making.seuhistory.com";
    	} else if($location.absUrl().indexOf('stage-making.seuhistory.com') >= 0) {
    		config.service_url = "https://stage-api-making.seuhistory.com";
    	} else if($location.absUrl().indexOf('making.seuhistory.com') >= 0) {
    		config.service_url = "https://api-making.seuhistory.com";
    	}
		moment.locale('pt-BR');
    	mdcDefaultParams({
			date 		: true,
			time 		: true,
			minutes 	: true,
			lang		: 'pt-BR',
			cancelText 	: 'Cancelar',
			todayText 	: 'Hoje',
			okText 		: 'OK',
			amText 		: 'AM',
			pmText		: 'PM',
			minuteSteps : 5,
			shortTime 	: true,
		});
		FastClick.attach(document.body);
		$rootScope.$on('loading', function(event, flag) {
			/// Bloqueia o loading geral. Geralmente para ser utilizado o loading do scroll.
			if(!$rootScope.inBackground) {
				if(flag) 	angular.element('div[id="loader"]').show();
				else 		angular.element('div[id="loader"]').hide();
			}
			$rootScope.loading = flag;
		});
		$rootScope.toggleMenu = function() {
			$mdSidenav('left').toggle();
		};
		$rootScope.logout = function() {
			$rootScope.inBackground = true;
			if(localStorage.getItem('authorization')) {
				$http({
					headers : {
						'Accept' : 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					method  : 'GET',
					url 	: config.service_url + '/api/v1/pt/cms/auth/logout'
				}).then(function(response) {
					$rootScope.inBackground = false;
				});
			}
			localStorage.removeItem('account');
			localStorage.removeItem('authorization');
			delete $rootScope.account;
			$state.go("login");
		};
		if(!(accountJson === undefined || accountJson === null || accountJson == 'undefined')) {
			$rootScope.account = JSON.parse(accountJson);
		}
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			$rootScope.$on('$stateChangeStart', stateChangeStartEvent);
		});
		$rootScope.$on('$stateChangeStart', stateChangeStartEvent);
		function stateChangeStartEvent (event, toState, toParams, fromState, fromParams) {
			delete $rootScope.inBackground;
			delete $rootScope.backPage;
			delete $rootScope.blockShowError;
			requestResponseStack = [];
			/// Remove todos os ouvintes de error, pra não ser disparado em outras telas.
			$rootScope.$offAll('error');
			$rootScope.$on('error', error);
			angular.element('input, textarea').blur();
			$rootScope.$offAll('$stateChangeStart');
		}
		$rootScope.backPageTo = function (to, params) {
			$rootScope.backPage = function () {
				angular.element('.content-container > div').removeClass('rtl');
				angular.element('.content-container > div').addClass('ltr');
				$timeout(function() { $state.go(to, params); }, 1);
			};
		};
		/// Quando não houver permissão.
		$rootScope.$on('unauthorized', function(event, data, status) {
			requestResponseStack = [];
			if(!$rootScope.blockShowError) {
				var message = !data.message || data.message == '' ? 'Você não possui permissão.' : data.message;
				alert(message, $rootScope.logout, 'Atenção', 'OK');
			}
			$rootScope.$broadcast('loading', false);
		});
		$rootScope.$on('request', function() {
			requestResponseStack.push('');
			$rootScope.$broadcast('loading', true);
		});
		$rootScope.$on('response', function() {
			requestResponseStack.pop();
			if(!requestResponseStack || !requestResponseStack.length)
				$rootScope.$broadcast('loading', false);
		});
		function error (event, data, status) {
			requestResponseStack = [];
			var message = '';
			if(data) {
				message = crackMessages(data.errors);
			}
			message = message != '' ? message : 'Houve um problema! Contacte-nos: <a href="mailto:suporte@14bits.com.br">suporte@14bits.com.br</a>.';
			if(!$rootScope.blockShowError) {
				alert(message, function() {
					if(status == 404) {
						$state.go('404');
					}
				}, 'Atenção', 'OK');
			} else {
				$rootScope.blockShowError = message;
			}
			$rootScope.$broadcast('loading', false);
		};
		function messagesModals () {
			$window.alert = function(message, resultFunction, title, buttonLabel) {
				var dialogShow;
				var alertDialog = $mdDialog.alert();
				alertDialog.parent(angular.element( document.body ));
				alertDialog.clickOutsideToClose(false);
				alertDialog.htmlContent(message);
				if(!title)
					title = 'Mensagem';
				alertDialog.title(title);
				alertDialog.ariaLabel(title);
				if(!buttonLabel)
					buttonLabel = 'OK';
				alertDialog.ok(buttonLabel);
	    		alertDialog.multiple(true);
				dialogShow = $mdDialog.show(alertDialog);
				if(resultFunction) {
					dialogShow.then(resultFunction);
				}
			};
			$window.confirm = function(message, successFunction, cancelFunction, title, okLabel, cancelLabel) {
				var dialogShow;
				var confirmDialog = $mdDialog.confirm();
				confirmDialog.parent(angular.element( document.body ));
				confirmDialog.clickOutsideToClose(false);
				confirmDialog.textContent(message);
				if(!title)
					title = 'Mensagem';
				confirmDialog.title(title);
				confirmDialog.ariaLabel(title);
				if(!okLabel)
					okLabel = 'OK';
				confirmDialog.ok(okLabel);
				if(!cancelLabel)
					cancelLabel = 'Cancelar';
				confirmDialog.multiple(true);
				confirmDialog.cancel(cancelLabel);
				dialogShow = $mdDialog.show(confirmDialog);
				if(successFunction || cancelFunction) {
					dialogShow.then(successFunction, cancelFunction);
				}
			};
			$window.prompt = function(message, successFunction, cancelFunction, title, okLabel, cancelLabel) {
				var dialogShow;
				var promptDialog = $mdDialog.prompt();
				promptDialog.parent(angular.element( document.body ));
				promptDialog.clickOutsideToClose(false);
				promptDialog.textContent(message);
				if(!title)
					title = 'Mensagem';
				promptDialog.title(title);
				promptDialog.ariaLabel(title);
				if(!okLabel)
					okLabel = 'OK';
				promptDialog.ok(okLabel);
				if(!cancelLabel)
					cancelLabel = 'Cancelar';
				promptDialog.multiple(true);
				promptDialog.cancel(cancelLabel);
				dialogShow = $mdDialog.show(promptDialog);
				if(successFunction || cancelFunction) {
					dialogShow.then(successFunction, cancelFunction);
				}
			};
		}
		messagesModals();
	})
	.run(function($rootScope, $state, $window){
	    $rootScope.$off = function (name, listener) {
	        var namedListeners = this.$$listeners[name];
	        if(namedListeners) {
	            for (var i = 0; i < namedListeners.length; i++) {
	                if(namedListeners[i] === listener) {
	                    return namedListeners.splice(i, 1);
	                }
	            }
	        }
	    }
	    $rootScope.$offAll = function (name) {
	        delete this.$$listeners[name];
	    }
	})
;