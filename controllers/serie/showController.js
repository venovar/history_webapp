"use strict";
application
	.controller('SerieShowController', function (
		$sce,
		$http,
		$state,
		$scope,
		$stateParams,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.selectedMode = 'md-scale';
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/serie/show',
				params	: {
					id : $stateParams.id,
				}
			}).then(function(response) {
				if(response.status == 200) {
					var serie = response.data.serie;
					$scope.serie = serie;
				}
			});
		}
		show();
		$scope.delete = function () {
			confirm('Deseja realmente remover esta Série?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/serie/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.serie,
				}).then(function(response) {
					if(response.status == 200) {
						alert(response.data.message, function() {
							$state.go('serieIndex');
						}, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
	})
;