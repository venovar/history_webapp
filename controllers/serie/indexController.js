"use strict";
application
	.controller('SerieIndexController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		config) 
	{
		$scope.filter = {};
		var filterSubmited;
		$scope.index = function (start, force) {
			if(start) {
				$scope.page = 1;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited))
					$scope.series = [];
				if(force) {
					$scope.series = [];
					filterSubmited = null;
				}
			}
			if($scope.page > 0) {
				$rootScope.inBackground = true;
				$scope.filter.page = $scope.page;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited)) {
					filterSubmited = angular.copy($scope.filter);
					$http({
						headers : {
							'Accept' : 'application/json',
							'Authorization' : localStorage.getItem('authorization')
						},
						method  : 'GET',
						url 	: config.service_url + '/api/v1/pt/cms/serie/index',
						params 	: $scope.filter
					}).then(function(response) {
						$rootScope.inBackground = false;
						if(response.status == 200) {
							$scope.series = $scope.series.concat(response.data.series.data);
							$scope.total = response.data.series.total;
							if(response.data.series.next_page_url != null) {
								$scope.page++; 
							} else {
								$scope.page = 0;
							}
						}
					});
				}
			}
		}
		$scope.index(true);
		$scope.delete = function (serie) {
			confirm('Deseja realmente remover esta Série?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/serie/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: serie,
				}).then(function(response) {
					if(response.status == 200) {
						$scope.index(true, true);
						alert(response.data.message, null, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
	})
;