"use strict";
application
	.controller('InstitutionalShowController', function (
		$sce,
		$http,
		$state,
		$scope,
		$stateParams,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.selectedMode = 'md-scale';
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/institutional/show',
				params	: {
					id : $stateParams.id,
					
				}
			}).then(function(response) {
				if(response.status == 200) {
					var institutional = response.data.institutional;
					$scope.institutional = institutional;
				}
			});
		}
		show();
		$scope.delete = function () {
			confirm('Deseja realmente remover esta Notificação Institucional?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/institutional/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.institutional,
				}).then(function(response) {
					if(response.status == 200) {
						alert(response.data.message, function() {
							$state.go('institutionalIndex');
						}, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
	})
;