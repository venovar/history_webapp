"use strict";
application
	.controller('InstitutionalStoreController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.institutional = {};
		// Opens the image modal to crop.
		$scope.imageCropDialog = function () {
			$mdDialog.show({
				controller 			: ImageCropController,
				templateUrl 		: 'views/dialog/imageCrop.html?v=' + Math.random(),
				clickOutsideToClose : true,
			})
			.then(function(image){
				$scope.institutional.image = image;
			})
			.then(function(){});
		}
		// Removes the main image of Institutional.
		$scope.removeImage = function () {
			confirm('Deseja realmente remover esta imagem?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/image/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.institutional.image,
				}).then(function(response) {
					if(response.status == 200) {
						delete $scope.institutional.image;
						alert(response.data.message, null, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
		// Opens the file explorer.
		$scope.chooseImage = function(event) {
	    	angular.element('#fileInput').click();
	    }
	    // Convert image object in markdown text.
	    function toMarkdown (image) {
	    	return '![](' + image.file + ')';
	    }
	    // Removes the markdown in text, if it hasn't been changed.
	    $scope.clearMarkdown = function (image) {
	    	if($scope.institutional.note && $scope.institutional.note != '')
	    		$scope.institutional.note = $scope.institutional.note.replace(toMarkdown(image), '');
	    }
	    // Insert the text in the cursor.
	    $scope.inputMarkdown = function (image) {
	    	var value = toMarkdown(image);
	    	var element = angular.element('textarea')[0];
	    	if(!$scope.institutional.note)
	    		$scope.institutional.note = '';
		    /*
		    /// IE
		    if (document.selection) {
		        element.focus();
		        sel = document.selection.createRange();
		        sel.text = value;
		    }
		    else */
		    if (element.selectionStart || element.selectionStart == '0') {
		        var startPos = element.selectionStart;
		        var endPos = element.selectionEnd;
		        $scope.institutional.note = $scope.institutional.note.substring(0, startPos)
		            + value
		            + $scope.institutional.note.substring(endPos, $scope.institutional.note.length);
		    } else {
		        $scope.institutional.note += value;
		    }
	    }
	    // Open the file explorer to choose a image. After that, will insert the markdown in text.
	    $scope.chooseImageInput = function(event) {
	    	if (event.target.files && event.target.files.length) {
    			var formData = new FormData();
			    formData.append("file", event.target.files[0]);
			    $http({
			    	headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization'),
						'Content-Type'	: undefined 
					},
					method  			: 'POST',
					url 				: config.service_url + '/api/v1/pt/cms/image_of_note/store',
					transformRequest 	: angular.identity,
					data 				: formData,
				}).then(function(response) {
					if(response.status == 200) {
						if(!$scope.institutional.image_of_notes || !$scope.institutional.image_of_notes.length)
							$scope.institutional.image_of_notes = [];
						$scope.institutional.image_of_notes.push(response.data.image_of_note);
						$scope.inputMarkdown(response.data.image_of_note);
					}
				});
			}
	    }
		// Removes the imagem and in sequence removes the markdown in the text.
		$scope.removeImageOfNote = function (image_of_note) {
			
			confirm('Deseja realmente remover esta imagem?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/image_of_note/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: image_of_note,
				}).then(function(response) {
					if(response.status == 200) {
						$scope.clearMarkdown(image_of_note);
						if($scope.institutional.image_of_notes && $scope.institutional.image_of_notes.length)
							$scope.institutional.image_of_notes.splice($scope.institutional.image_of_notes.indexOf(image_of_note), 1);
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
		// Create the Institutional.
		$scope.submit = function (redirect) {
			$http({
				url 				: config.service_url + '/api/v1/pt/cms/institutional/store',
				method  			: 'POST',
				headers : {
					'Accept' 		: 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				data 				: datesFormat($scope.institutional),
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						if(redirect)
							$state.go('institutionalShow', response.data.institutional);
					}, 'Sucesso');
				}
			});
		}
	})
;