"use strict";
function ImageCropController (
		$http,
		$state,
		$scope,
		$rootScope,
		$mdDialog,
		config) {
	
	$scope.inputImage;
        
    $scope.fileInputChange = function(event) {
    	if(event.target.files[0].type && event.target.files[0].type.indexOf('image') >= 0) {
	    	var reader = new FileReader();
			reader.onload = function(event) {
				$scope.inputImage = event.target.result;
				$scope.$apply();
			}
			reader.readAsDataURL(event.target.files[0]);
    	} else {
    		alert('Arquivo inválido. Utilize somente imagens.', null, 'Atenção');
    	}
    }
    $scope.cancel = function () {
    	$mdDialog.cancel();
    }
    $scope.upload = function () {
    	$http({
			headers : {
				'Accept' 		: 'application/json',
				'Authorization' : localStorage.getItem('authorization')
			},
			method  			: 'POST',
			url 				: config.service_url + '/api/v1/pt/cms/image/store',
			data 				: {
				base64 : $scope.outputImage
			},
		}).then(function(response) {
			if(response.status == 200) {
				$mdDialog.hide(response.data.image);
			}
		});
    }
};