"use strict";
application
	.controller('AppLogoStoreController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.app_logo = config.service_url + '/api/v1/pt/cms/app_logo/show?v=' + Math.random();
		$scope.chooseImage = function(event) {
	    	angular.element('#fileInput').click();
	    }
	    $scope.chooseImageInput = function(event) {
	    	if (event.target.files && event.target.files.length) {
    			var formData = new FormData();
			    formData.append("file", event.target.files[0]);
			    formData.append("formats[]", 'png');
			    formData.append("formats[]", 'jpeg');
			    $http({
			    	headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization'),
						'Content-Type'	: undefined 
					},
					method  			: 'POST',
					url 				: config.service_url + '/api/v1/pt/cms/app_logo/store',
					transformRequest 	: angular.identity,
					data 				: formData,
				}).then(function(response) {
					if(response.status == 200) {
						alert(response.data.message, null, 'Sucesso');
						$scope.app_logo = config.service_url + '/api/v1/pt/cms/app_logo/show?v=' + Math.random();
					}
				});
			}
	    }
	})
;