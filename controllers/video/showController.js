"use strict";
application
	.controller('VideoShowController', function (
		$sce,
		$http,
		$state,
		$scope,
		$stateParams,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.selectedMode = 'md-scale';
		
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/video/show',
				params	: {
					id : $stateParams.id
				}
			}).then(function(response) {
				if(response.status == 200) {
					$scope.video = response.data.video;
				}
			});
		}
		show();
		
		$scope.delete = function () {
			
			confirm('Deseja realmente remover este Vídeo?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/video/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.video,
				}).then(function(response) {
					if(response.status == 200) {
						alert(response.data.message, function() {
							$state.go('videoIndex');
						}, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
			
		}
	})
;