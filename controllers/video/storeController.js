"use strict";
application
	.controller('VideoStoreController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.video = {};
		$scope.imageCropDialog = function () {
			$mdDialog.show({
				controller 			: ImageCropController,
				templateUrl 		: 'views/dialog/imageCrop.html?v=' + Math.random(),
				clickOutsideToClose : true,
			})
			.then(function(image){
				$scope.video.image = image;
			})
			.then(function(){});
		}
		$scope.removeImage = function () {
			confirm('Deseja realmente remover esta imagem?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/image/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.video.image,
				}).then(function(response) {
					if(response.status == 200) {
						delete $scope.video.image;
						alert(response.data.message, null, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
		$scope.submit = function () {
			$http({
				url 				: config.service_url + '/api/v1/pt/cms/video/store',
				method  			: 'POST',
				headers : {
					'Accept' 		: 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				data 				: $scope.video,
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						$state.go('videoIndex');
					}, 'Sucesso');
				}
			});
		}
	})
;