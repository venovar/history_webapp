"use strict";
application
	.controller('GifUpdateController', function (
		$stateParams,
		$rootScope,
		$mdDialog,
		$state,
		$scope,
		$http,
		config) 
	{
		$scope.gif = {};
		
	    $scope.chooseImage = function(event) {
	    	angular.element('#fileInput').click();
	    }
	    $scope.chooseImageInput = function(event) {
	    	if (event.target.files && event.target.files.length) {
    			var formData = new FormData();
			    formData.append("file", event.target.files[0]);
			    formData.append("formats[]", 'gif');
			    $http({
			    	headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization'),
						'Content-Type'	: undefined 
					},
					method  			: 'POST',
					url 				: config.service_url + '/api/v1/pt/cms/image/store',
					transformRequest 	: angular.identity,
					data 				: formData,
				}).then(function(response) {
					if(response.status == 200) {
						$scope.gif.image = response.data.image;
					}
				});
			}
	    }
		$scope.removeImage = function () {
			confirm('Deseja realmente remover esta imagem?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/image/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.gif.image,
				}).then(function(response) {
					if(response.status == 200) {
						delete $scope.gif.image;
						alert(response.data.message, null, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
			
		}
		$scope.submit = function () {
			$http({
				url 				: config.service_url + '/api/v1/pt/cms/gif/update',
				method  			: 'PUT',
				headers : {
					'Accept' 		: 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				data 				: $scope.gif,
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						$state.go('gifIndex');
					}, 'Sucesso');
				}
			});
		}
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/gif/show',
				params	: {
					id : $stateParams.id
				}
			}).then(function(response) {
				if(response.status == 200) {
					$scope.gif = response.data.gif;
				}
			});
		}
		show();
	})
;