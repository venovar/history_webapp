"use strict";
application
	.controller('GifIndexController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		config) 
	{
		$scope.filter = {}
		var filterSubmited;
		$scope.index = function (start, force) {
			if(start) {
				$scope.page = 1;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited))
					$scope.gifs = [];
				if(force) {
					$scope.gifs = [];
					filterSubmited = null;
				}
			}
			if($scope.page > 0) {
				$rootScope.inBackground = true;
				$scope.filter.page = $scope.page;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited)) {
					filterSubmited = angular.copy($scope.filter);
					$http({
						headers : {
							'Accept' : 'application/json',
							'Authorization' : localStorage.getItem('authorization')
						},
						method  : 'GET',
						url 	: config.service_url + '/api/v1/pt/cms/gif/index',
						params 	: $scope.filter
					}).then(function(response) {
						$rootScope.inBackground = false;
						if(response.status == 200) {
							$scope.gifs = $scope.gifs.concat(response.data.gifs.data);
							$scope.total = response.data.gifs.total;
							if(response.data.gifs.next_page_url != null) {
								$scope.page++; 
							} else {
								$scope.page = 0;
							}
						}
					});
				}
			}
		}
		$scope.index(true);
		
		$scope.delete = function (gif) {
			
			confirm('Deseja realmente remover este Gif?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/gif/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: gif,
				}).then(function(response) {
					if(response.status == 200) {
						$scope.index(true, true);
						alert(response.data.message, null, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
			
		}
	})
;