"use strict";
application
	.controller('GifShowController', function (
		$sce,
		$http,
		$state,
		$scope,
		$stateParams,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.selectedMode = 'md-scale';
		
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/gif/show',
				params	: {
					id : $stateParams.id
				}
			}).then(function(response) {
				if(response.status == 200) {
					var gif = response.data.gif;
					gif.embed = $sce.trustAsHtml(gif.embed);
					$scope.gif = gif;
				}
			});
		}
		show();
		
		$scope.delete = function () {
			
			confirm('Deseja realmente remover este Gif?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/gif/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.gif,
				}).then(function(response) {
					if(response.status == 200) {
						alert(response.data.message, function() {
							$state.go('gifIndex');
						}, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
			
		}
	})
;