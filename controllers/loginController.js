"use strict";
application
	.controller('LoginController', function (
		$http,
		$state,
		$scope,
		$window,
		$timeout,
		$mdDialog,
		$rootScope,
		$stateParams,
		config) 
	{
		if($rootScope.account) {
			$state.go('highlightIndex');
		}
		function facebookDialog () {
			$mdDialog.show({
				controller 			: function($mdDialog, $scope) {
					$scope.hide = $mdDialog.hide;
				},
				templateUrl 		: 'views/page/login/dialog/facebook.html?v=' + Math.random(),
				parent 				: angular.element('.login-page'),
				clickOutsideToClose : false,
			})
			.then(facebook);
		}
		function storesDialog () {
			$mdDialog.show({
				templateUrl 		: 'views/page/login/dialog/stores.html?v=' + Math.random(),
				parent 				: angular.element('.login-page'),
				clickOutsideToClose : false,
			});
		}
		function facebook () {
			$rootScope.blockShowError = true;
			$http({
					url 	: config.service_url + '/api/v1/pt/auth/facebook',
					method  : 'POST',
					headers : {
						'Accept' : 'application/json',
					},
					data : {
						code : $stateParams.code
					}
				}).then(function(response) {
					if(response.status == 200) {
						/// Redirect
						if(response.data.redirect != undefined && response.data.redirect != '') {
							$window.location.href = response.data.redirect;
						}
						/// Entra na aplicação.
						else if(response.data && response.data.access && response.data.access.redirect == 'cms') {
							localStorage.setItem(
								'authorization', 
								response.data.access.token_type + ' ' + response.data.access.access_token
							);
							localStorage.setItem(
								'account', 
								JSON.stringify(response.data.user)
							);
							$rootScope.account = response.data.user;
							$state.go('highlightIndex');
						} 
						/// Notifica o usuário para usar a versão mobile.
						else {
							storesDialog();
						}
					} else {
						alert($rootScope.blockShowError , function() {
							delete $rootScope.blockShowError;
							facebookDialog();
						}, 'Atenção', 'OK');
					}
				});
		}
		if($stateParams.code && $stateParams.code != '') {
			facebook();
		} else {
			facebookDialog();
		}
	})
;