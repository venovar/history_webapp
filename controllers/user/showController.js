"use strict";
application
	.controller('UserShowController', function (
		$sce,
		$http,
		$state,
		$scope,
		$stateParams,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.selectedMode = 'md-scale';
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/user/show',
				params	: {
					id : $stateParams.id,
				}
			}).then(function(response) {
				if(response.status == 200) {
					var user = response.data.user;
					$scope.user = user;
					delete $rootScope.blockShowError;
				}
			});
		}
		show();
		$scope.delete = function () {
			confirm('Deseja realmente remover este Usuário?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/user/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.user,
				}).then(function(response) {
					if(response.status == 200) {
						alert(response.data.message, function() {
							if($scope.user.id == $rootScope.account.id) {
								$rootScope.logout();
							} else {
								$state.go('userIndex');
							}
						}, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
		$scope.update = function () {
			$http({
				url 				: config.service_url + '/api/v1/pt/cms/user/update',
				method  			: 'PUT',
				headers : {
					'Accept' 		: 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				data 				: {
					id 		: $scope.user.id,
					admin 	: $scope.user.admin,
					enabled : $scope.user.enabled,
				},
			}).then(function(response) {
				if(response.status == 200) {
					$scope.user = response.data.user;
					alert(response.data.message, null, 'Sucesso');
				}
			});
		}
	})
;