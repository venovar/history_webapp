"use strict";
application
	.controller('UserIndexController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		config) 
	{
		var filterSubmited;
		$scope.filter = {};
		$scope.index = function (start, force) {
			if(start) {
				$scope.page = 1;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited))
					$scope.users = [];
				if(force) {
					$scope.users = [];
					filterSubmited = null;
				}
			}
			if($scope.page > 0) {
				$rootScope.inBackground = true;
				$scope.filter.page = $scope.page;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited)) {
					filterSubmited = angular.copy($scope.filter);
					$http({
						headers : {
							'Accept' : 'application/json',
							'Authorization' : localStorage.getItem('authorization')
						},
						method  : 'GET',
						url 	: config.service_url + '/api/v1/pt/cms/user/index',
						params 	: $scope.filter
					}).then(function(response) {
						$rootScope.inBackground = false;
						if(response.status == 200) {
							$scope.users = $scope.users.concat(response.data.users.data);
							$scope.total = response.data.users.total;
							if(response.data.users.next_page_url != null) {
								$scope.page++; 
							} else {
								$scope.page = 0;
							}
						}
					});
				}
			}
		}
		$scope.index(true);
		$scope.delete = function (user) {
			confirm('Deseja realmente remover este Usuário?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/user/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: user,
				}).then(function(response) {
					if(response.status == 200) {
						alert(response.data.message, function() {
							if(user.id == $rootScope.account.id) {
								$rootScope.logout();
							} else {
								$scope.index(true, true);
							}
						}, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
	})
;