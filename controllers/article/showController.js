"use strict";
application
	.controller('ArticleShowController', function (
		$sce,
		$http,
		$state,
		$scope,
		$stateParams,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.selectedMode = 'md-scale';
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/article/show',
				params	: {
					id : $stateParams.id,
					
				}
			}).then(function(response) {
				if(response.status == 200) {
					var article = response.data.article;
					$scope.article = article;
				}
			});
		}
		show();
		$scope.delete = function () {
			confirm('Deseja realmente remover este Artigo?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/article/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.article,
				}).then(function(response) {
					if(response.status == 200) {
						alert(response.data.message, function() {
							$state.go('articleIndex');
						}, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
	})
;