"use strict";
application
	.controller('ArticleUpdateController', function (
		$http,
		$state,
		$scope,
		$stateParams,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.article = {};
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/article/show',
				params	: {
					id 			: $stateParams.id,
					markdown 	: 0,
				}
			}).then(function(response) {
				if(response.status == 200) {
					if(response.data.article.conveyed_at && response.data.article.conveyed_at != '')
						response.data.article.conveyed_at = moment(response.data.article.conveyed_at, "YYYY-MM-DD H:m:s").toDate();
					$scope.article = response.data.article;
				}
			});
		}
		show();
		// Opens the image modal to crop.
		$scope.imageCropDialog = function () {
			$mdDialog.show({
				controller 			: ImageCropController,
				templateUrl 		: 'views/dialog/imageCrop.html?v=' + Math.random(),
				clickOutsideToClose : true,
			})
			.then(function(image){
				$scope.article.image = image;
			})
			.then(function(){});
		}
		// Removes the main image of Article.
		$scope.removeImage = function () {
			
			confirm('Deseja realmente remover esta imagem?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/image/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.article.image,
				}).then(function(response) {
					if(response.status == 200) {
						delete $scope.article.image;
						alert(response.data.message, null, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
	    // Convert image object in markdown text.
	    function toMarkdown (image) {
	    	return '![](' + image.file + ')';
	    }
	    // Removes the markdown in text, if it hasn't been changed.
	    $scope.clearMarkdown = function (image) {
	    	if($scope.article.note && $scope.article.note != '')
	    		$scope.article.note = $scope.article.note.replace(toMarkdown(image), '');
	    }
	    // Insert the text in the cursor.
	    $scope.inputMarkdown = function (image) {
	    	var value = toMarkdown(image);
	    	var element = angular.element('textarea')[0];
	    	if(!$scope.article.note)
	    		$scope.article.note = '';
		    /*
		    /// IE
		    if (document.selection) {
		        element.focus();
		        sel = document.selection.createRange();
		        sel.text = value;
		    }
		    else */
		    if (element.selectionStart || element.selectionStart == '0') {
		        var startPos = element.selectionStart;
		        var endPos = element.selectionEnd;
		        $scope.article.note = $scope.article.note.substring(0, startPos)
		            + value
		            + $scope.article.note.substring(endPos, $scope.article.note.length);
		    } else {
		        $scope.article.note += value;
		    }
	    }
		// Opens the file explorer.
		$scope.chooseImage = function(event) {
	    	angular.element('#fileInput').click();
	    }
	    // Open the file explorer to choose a image. After that, will insert the markdown in text.
	    $scope.chooseImageInput = function(event) {
	    	if (event.target.files && event.target.files.length) {
    			var formData = new FormData();
			    formData.append("file", event.target.files[0]);
			    $http({
			    	headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization'),
						'Content-Type'	: undefined 
					},
					method  			: 'POST',
					url 				: config.service_url + '/api/v1/pt/cms/image_of_note/store',
					transformRequest 	: angular.identity,
					data 				: formData,
				}).then(function(response) {
					if(response.status == 200) {
						if(!$scope.article.image_of_notes || !$scope.article.image_of_notes.length)
							$scope.article.image_of_notes = [];
						$scope.article.image_of_notes.push(response.data.image_of_note);
						$scope.inputMarkdown(response.data.image_of_note);
					}
				});
			}
	    }
		// Removes the imagem and in sequence removes the markdown in the text.
		$scope.removeImageOfNote = function (image_of_note) {
			confirm('Deseja realmente remover esta imagem?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/image_of_note/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: image_of_note,
				}).then(function(response) {
					if(response.status == 200) {
						$scope.clearMarkdown(image_of_note);
						if($scope.article.image_of_notes && $scope.article.image_of_notes.length)
							$scope.article.image_of_notes.splice($scope.article.image_of_notes.indexOf(image_of_note), 1);
						$scope.submit(false);
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
		}
		// Create the Article.
		$scope.submit = function (redirect) {
			$http({
				url 				: config.service_url + '/api/v1/pt/cms/article/update',
				method  			: 'PUT',
				headers : {
					'Accept' 		: 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				data 				: datesFormat($scope.article),
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						if(redirect)
							$state.go('articleIndex');
					}, 'Sucesso');
				}
			});
		}
	})
;