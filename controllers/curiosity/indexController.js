"use strict";
application
	.controller('CuriosityIndexController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		config) 
	{
		$scope.filter = {};
		var filterSubmited;
		$scope.index = function (start, force) {
			if(start) {
				$scope.page = 1;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited))
					$scope.curiosities = [];
				if(force) {
					$scope.curiosities = [];
					filterSubmited = null;
				}
			}
			if($scope.page > 0) {
				$rootScope.inBackground = true;
				$scope.filter.page = $scope.page;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited)) {
					filterSubmited = angular.copy($scope.filter);
					var filter = angular.copy($scope.filter);
					if(filter.init && filter.init.length != 0) {
						var init = filter.init.split('/');
						filter.init = init.reverse().join('-');
					}
					if(filter.end && filter.end.length != 0) {
						var end = filter.end.split('/');
						filter.end = end.reverse().join('-');
					}
					$http({
						headers : {
							'Accept' : 'application/json',
							'Authorization' : localStorage.getItem('authorization')
						},
						method  : 'GET',
						url 	: config.service_url + '/api/v1/pt/cms/curiosity/index',
						params 	: filter
					}).then(function(response) {
						$rootScope.inBackground = false;
						if(response.status == 200) {
							$scope.curiosities = $scope.curiosities.concat(response.data.curiosities.data);
							$scope.total = response.data.curiosities.total;
							if(response.data.curiosities.next_page_url != null) {
								$scope.page++; 
							} else {
								$scope.page = 0;
							}
						}
					});
				}
			}
		}
		$scope.index(true);
		
		$scope.delete = function (curiosity) {
			
			confirm('Deseja realmente remover este "Hoje na História"?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/curiosity/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: curiosity,
				}).then(function(response) {
					if(response.status == 200) {
						$scope.index(true, true);
						alert(response.data.message, null, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
			
		}
	})
;