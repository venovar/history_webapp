"use strict";
application
	.controller('CuriosityShowController', function (
		$sce,
		$http,
		$state,
		$scope,
		$stateParams,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.selectedMode = 'md-scale';
		
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/pt/cms/curiosity/show',
				params	: {
					id : $stateParams.id,
					
				}
			}).then(function(response) {
				if(response.status == 200) {
					var curiosity = response.data.curiosity;
					$scope.curiosity = curiosity;
				}
			});
		}
		show();
		
		$scope.delete = function () {
			
			confirm('Deseja realmente remover este "Hoje na História"?', function() {
				$http({
					url 				: config.service_url + '/api/v1/pt/cms/curiosity/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					params 				: $scope.curiosity,
				}).then(function(response) {
					if(response.status == 200) {
						alert(response.data.message, function() {
							$state.go('curiosityIndex');
						}, 'Sucesso');
					}
				});
			}, null, 'Atenção', 'Sim', 'Não');
			
		}
	})
;