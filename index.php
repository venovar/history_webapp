<!DOCTYPE html>
<html>
<head>
    <title>History</title>
    <meta charset="utf-8"/>
    <meta name="format-detection" content="telephone=no" />
    <meta name="msapplication-tap-highlight" content="no" />
    <base href="/"/>
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width" />
    <!-- STYLES -->
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="bower_components/angular-material/angular-material.css"/>
    <link rel="stylesheet" href="bower_components/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="bower_components/croppie/croppie.css">
    <link rel="stylesheet" href="bower_components/ngCroppie/ng-croppie.css">
    <link rel="stylesheet" href="bower_components/ng-material-datetimepicker/dist/material-datetimepicker.min.css">
    
    <link rel="icon" href="img/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="img/icon60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/icon76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/icon120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/icon152.png">
    <link rel="apple-touch-icon" sizes="175x175" href="img/icon175.png">
</head>
<body ng-app="Application" ng-controller="RootController">
    <div layout="row" layout-fill layout-align="none none">
        <div layout="column" flex flex-order="2">
            <div class="alert-target content-container" flex style="position: relative;" flex-order="5">
                <div 
                    ui-view="content"
                    class="rtl layout-fill" 
                    style="position: absolute; top: 0; left: 0;"></div>
            </div>
            <div ui-view="menuBar" flex-order="4"></div>
            <div ui-view="navigationBar" flex-order="3"></div>
        </div>
        <div ui-view="sideNav" hide-gt-md flex-order="1"></div>
    </div>
    <div id="loader" layout="row" layout-align="center center">
        <md-progress-circular md-mode="indeterminate" class="md-accent"></md-progress-circular>
    </div>
    <!-- SCRIPTS -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>
    <script src="bower_components/angular/angular.js"></script>
    <script src="bower_components/angular-aria/angular-aria.js"></script>
    <script src="bower_components/angular-animate/angular-animate.js"></script>
    <script src="bower_components/angular-material/angular-material.js"></script>
    <script src="bower_components/angular-messages/angular-messages.js"></script>    
    <script src="bower_components/angular-sanitize/angular-sanitize.js"></script>
    <script src="bower_components/angular-route/angular-route.js"></script>
    <script src="bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="bower_components/angular-ui-mask/dist/mask.min.js"></script>
    <script src="bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="bower_components/moment/min/moment.min.js"></script>
    <script src="bower_components/moment/locale/pt-br.js"></script>
    <script src="bower_components/moment/min/moment-with-locales.js"></script>
    <script src="bower_components/moment-timezone/builds/moment-timezone-with-data.min.js"></script>
    <script src="bower_components/fastclick/lib/fastclick.js"></script>
    <script src="bower_components/croppie/croppie.js"></script>
    <script src="bower_components/ngCroppie/ng-croppie.js"></script>
    <script src="bower_components/ng-material-datetimepicker/dist/angular-material-datetimepicker.min.js"></script>
    
    <?php $random = rand(); ?>

    <script src="js/app.js?v=<?php echo $random ?>"></script>
    <script src="js/services.js?v=<?php echo $random ?>"></script>
    <script src="js/configs.js?v=<?php echo $random ?>"></script>
    <script src="js/directives.js?v=<?php echo $random ?>"></script>
    <script src="js/filters.js?v=<?php echo $random ?>"></script>
    <script src="js/routes.js?v=<?php echo $random ?>"></script>
    <script src="js/utils.js?v=<?php echo $random ?>"></script>
    
    <?php
        function scanDirRecursive ($dir, $random) {
            $dirCtrls = scandir($dir);
            foreach ($dirCtrls as $value) {
                if($value != '.' && $value != '..') {
                    if(is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                        scanDirRecursive($dir . DIRECTORY_SEPARATOR . $value, $random);
                    } else if($value != '.DS_Store'){
                        echo sprintf("<script src='%s/%s?v=%s'></script>", $dir, $value, $random);
                    }
                }
            }
        }
        scanDirRecursive("controllers", $random);
    ?>
</body>
</html>